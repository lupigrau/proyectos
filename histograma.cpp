#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>
#include "text.h"

#define NUM_LET ('z' - 'a' +1)

int 
main (int argc, char *argv[])
{

	unsigned counter[NUM_LET];
	const char *pletter = text;

	bzero (counter, NUM_LET * sizeof (unsigned));

	while (*pletter != '\0') {
		if (isalpha(*pletter))
			counter[tolower(*pletter)-'a']++;
		pletter++;
	}

	printf ("\n");
        for (int i=0; i<NUM_LET; i++)
                printf ("%c: %u\n", i + 'A', counter[i]);
        printf ("\n");


                return EXIT_SUCCESS;




}
